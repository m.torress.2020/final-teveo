from django.contrib import admin
from .models import Camera, Comment, Like, User

# Register your models here.
admin.site.register(Camera)
admin.site.register(Comment)
admin.site.register(User)
admin.site.register(Like)
