from django.shortcuts import render, redirect
import base64
from django.http import HttpResponse, HttpResponseRedirect
import random
from operator import attrgetter
import json
import uuid
import urllib.request
from django.views.decorators.csrf import csrf_exempt
from .models import Camera, Comment, User, Like
from datetime import datetime
import xml.etree.ElementTree as ET
from django.core.paginator import Paginator


ID_1 = "LIS1-"
ID_2 = "LIS2-"
ID_3 = "LIS3-"


headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:100.0) Gecko/20100101 Firefox/100.0'
}


def process_xml1():
    tree = ET.parse('listado1.xml') # abre el xml y crea un árbol de jerarquía
    root = tree.getroot() # devuelve el elemento raíz del xml

    for camara in root.findall('camara'):
        camera_id = ID_1 + camara.find('id').text
        url = camara.find('src').text
        location = camara.find('lugar').text
        coordenadas = camara.find('coordenadas').text
        longitude, latitude = coordenadas.split(',')

        if not Camera.objects.filter(camera_id=camera_id).exists():
            Camera.objects.create(
                camera_id=camera_id,
                url=url,
                location=location,
                latitude=latitude,
                longitude=longitude,
            )


def process_xml2():
    tree = ET.parse('listado2.xml')
    root = tree.getroot()

    for cam in root.findall('cam'):
        camera_id = ID_2 + cam.attrib['id']  # <cam id="A">
        url = cam.find('url').text   # <cam>  <url>
        info = cam.find('info').text
        latitude = cam.find('place/latitude').text  #  <cam>  <place>  <latitude>
        longitude = cam.find('place/longitude').text

        if not Camera.objects.filter(camera_id=camera_id).exists():
            Camera.objects.create(
                camera_id=camera_id,
                url=url,
                location=info,
                latitude=latitude,
                longitude=longitude,
            )

def process_xml3():
    tree = ET.parse('listado3.xml')
    root = tree.getroot()

    for row in root.findall('row'):
        camera_id = ID_3 + row.find('index').text
        name = row.find('title').text
        url = row.find('urlcam').text
        latitude = row.find('latwgs84').text
        longitude = row.find('lonwgs84').text
        road = row.find('road').text

        if not Camera.objects.filter(camera_id=camera_id).exists():
            Camera.objects.create(
                camera_id=camera_id,
                url=url,
                location=f"{name}, {road}",
                latitude=latitude,
                longitude=longitude,
            )


def download_image(url):
    """Download image and return it as bytes"""
    # crea un objeto Request para realizar una solicitud HTTP
    request = urllib.request.Request(url=url, headers=headers)
    try:
        # hace la solicitud HTTP y almacena la respuesta HTTP en response
        with urllib.request.urlopen(request) as response:
            # lee el cuerpo de response (bytes de imagen)
            image = response.read()
    except urllib.error.URLError:
        return None
    return image


@csrf_exempt
def logout(request):
    if request.method == "POST":
        if request.POST.get('action') == 'logout':
            response = redirect('/')
            response.delete_cookie('session_id')
            user = create_cookie(request)
            response.set_cookie('session_id', user.session_id)
            return response
    return redirect(request.path)


def image_embedded(request, camera_id):
    """Return HTML text, with an IMG element embedding the image"""
    camera = Camera.objects.get(camera_id=camera_id)
    image = download_image(camera.url)
    if image is None:
        return HttpResponse(str(urllib.error.URLError), status=500)
    else:
        html=''
        if image is not None:
            # decode: pasa de bytes a str -- 64 caracteres ascii
            image_base64 = base64.b64encode(image).decode('utf-8')
            html = f'<img src="data:image/jpeg;base64,{image_base64}">'
        return HttpResponse(html, content_type="text/html")


def comment_dyn(request, camera_id):
    camera = Camera.objects.get(camera_id=camera_id)
    comments = Comment.objects.filter(camera=camera).order_by("-date")

    for comment in comments:
        if comment.image is not None:
            comment.image = base64.b64encode(comment.image).decode('utf-8')

    html = ''
    for comment in comments:
        comment.date = comment.date.strftime("%Y-%m-%d %H:%M:%S")
        html += f'''<div class="comment-box row mt-3">
                        <div class="col-md-6">
                            <h5>Usuario: { comment.user }</h5>
                            <p>{ comment.text }</p>
                            <hr>
                            <p>Fecha: { comment.date }</p>
                        </div>
                        <div class="col-md-6">
                            <img src="data:image/jpeg;base64,{ comment.image }" class="img-comment">
                        </div>
                    </div>'''

    return HttpResponse(html, content_type="text/html")


def camera_json(request):
    camera_id = request.GET.get('camera_id')
    camera = Camera.objects.get(camera_id=camera_id)
    camera.ncomments = Comment.objects.filter(camera=camera).count()
    info = {
        'camera_id': camera.camera_id,
        'url': camera.url,
        'location': camera.location,
        'latitude': camera.latitude,
        'longitude': camera.longitude,
        'ncomments': camera.ncomments,
    }
    return HttpResponse(json.dumps(info), content_type='application/json')


def create_cookie(request):
    # uuid4: hexa 8-4-4-4-12    hex: 32 seguidos (quita guiones)
    session_id = uuid.uuid4().hex
    user = User.objects.create(
        session_id=session_id,
        font="Calibri",
        font_size="24px",
        username="Anónimo",
    )
    return user


def get_session(request):
    if 'session_id' not in request.COOKIES:
        user = create_cookie(request)
        cookie = redirect('/')
        cookie.set_cookie("session_id", user.session_id)
    else:
        session_id = request.COOKIES.get("session_id")
        try:
            user = User.objects.get(session_id=session_id)
        except User.DoesNotExist:
            user = create_cookie(request)
            cookie = redirect('/')
            cookie.set_cookie("session_id", session_id)
    return user


def index(request):
    if 'session_id' not in request.COOKIES or not User.objects.filter(session_id=request.COOKIES.get("session_id")):
        user = create_cookie(request)
        cookie = redirect('/')
        cookie.set_cookie("session_id", user.session_id)
        return cookie
    else:
        session_id = request.COOKIES.get("session_id")
        user = User.objects.get(session_id=session_id)

    comments = Comment.objects.all().order_by('-date')

    for comment in comments:
        if comment.image is not None:
            comment.image = base64.b64encode(comment.image).decode('utf-8')

    paginator = Paginator(comments, 5)
    page_number = request.GET.get('page') # número de página que se solicita /?page=2
    page_obj = paginator.get_page(page_number)
    # objeto Page que contiene los comments, tiene métodos como: has_previous has_next
    # page_obj.next_page_number  page_obj.previous_page_number
    # page_obj.paginator.num_pages
    # page_obj.number NÚMERO PÁGINA ACTUAL

    cameras = Camera.objects.all()

    context = {
        'user': user,
        'camaras': cameras,
        'comments': comments,
        'page_obj': page_obj,
        'ncameras': cameras.count(),
        'ncomments': comments.count(),
    }
    return render(request, 'index.html', context)


@csrf_exempt
def cameras_page(request):
    cameras = Camera.objects.all()
    comments = Comment.objects.all()
    user = get_session(request)

    if request.method == 'POST':
        list = request.POST.get('listado')
        if list == 'listado1':
            process_xml1()
        elif list == 'listado2':
            process_xml2()
        elif list == 'listado3':
            process_xml3()

    image_base64 = b''
    random_camera = None

    if cameras:
        random_camera = random.choice(cameras)
        random_image = download_image(random_camera.url)
        if random_image is not None:
            image_base64 = base64.b64encode(random_image).decode('utf-8')

    for camera in cameras:
        camera.ncomments = Comment.objects.filter(camera=camera).count()
        camera.nlikes = Like.objects.filter(camera=camera).count()

    sorted_cameras = sorted(cameras, key=attrgetter('ncomments'), reverse=True)

    context = {
        'user': user,
        'cameras': sorted_cameras,
        'ncameras': cameras.count(),
        'ncomments': comments.count(),
        'random_image': image_base64,
        'random_camera': random_camera,
    }

    return render(request, "cameras_page.html", context)


@csrf_exempt
def single_camera_page(request, camera_id):
    camera = Camera.objects.get(camera_id=camera_id)
    url = camera.url
    image = download_image(url)

    image_base64 = ''
    if image is not None:
        image_base64 = base64.b64encode(image).decode('utf-8')

    user = get_session(request)

    comments = Comment.objects.filter(camera=camera).order_by("-date")

    for comment in comments:
        if comment.image is not None:
            comment.image = base64.b64encode(comment.image).decode('utf-8')

    if request.method == "POST":
        if request.POST.get('action') == 'Like':
            if not Like.objects.filter(camera=camera, user=user).exists():
                Like.objects.create(camera=camera, user=user)

    nlikes = Like.objects.filter(camera=camera).count()

    paginator = Paginator(comments, 2)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    context = {
        'image': image_base64,
        'user': user,
        'camera': camera,
        'comments': comments,
        'page_obj': page_obj,
        'ncameras': Camera.objects.all().count(),
        'ncomments': Comment.objects.all().count(),
        'nlikes': nlikes,
    }
    return render(request, "single_camera_page.html", context)

def dynamic_page(request, camera_id):
    user = get_session(request)
    camera = Camera.objects.get(camera_id=camera_id)
    comments = Comment.objects.filter(camera=camera).order_by("-date")

    context = {
        'user': user,
        'camera': camera,
        'comments': comments,
        'ncameras': Camera.objects.all().count(),
        'ncomments': Comment.objects.all().count(),
    }
    return render(request, "dynamic.html", context)


@csrf_exempt
def config_page(request):
    user = get_session(request)
    session_id = user.session_id
    url_session = request.build_absolute_uri(f"?id={session_id}")

    id_navigator = request.GET.get('id')

    if id_navigator and id_navigator != session_id:
        response = HttpResponseRedirect(request.path)
        response.set_cookie('session_id', id_navigator)
        return response

    if request.method == "POST":
        if request.POST['action'] == 'change-user':
            user.username = request.POST['username']
        elif request.POST['action'] == 'change-font':
            user.font = request.POST.get('font', user.font)
            user.font_size = request.POST.get('font-size', user.font_size)
        user.save()

    context = {
        'user': user,
        'ncameras': Camera.objects.all().count(),
        'ncomments': Comment.objects.all().count(),
        'url_session': url_session,
    }
    return render(request, "config.html", context)


def help_page(request):
    user = get_session(request)
    cameras = Camera.objects.all()
    comments = Comment.objects.all()
    context = {
        'user': user,
        'ncameras': cameras.count(),
        'ncomments': comments.count(),
    }
    return render(request, "help.html", context)


@csrf_exempt
def comment_page(request):
    user = get_session(request)

    context = {
        'user': user,
        'date': datetime.now(),
        'ncameras': Camera.objects.all().count(),
        'ncomments': Comment.objects.all().count(),
    }

    camera_id = request.GET.get('camera_id')
    if Camera.objects.filter(camera_id=camera_id):
        camera = Camera.objects.get(camera_id=camera_id)
        context['camera'] = camera
        image = download_image(camera.url)
        if image is not None:
            image_base64 = base64.b64encode(image).decode('utf-8')
            context['image'] = image_base64
        else:
            image = b''
        if request.method == 'POST':
            if request.POST['action'] == 'make-comment':
                text = request.POST.get('text', '')
                return_url = request.POST.get('return_url', '/')
                Comment.objects.create(
                    text=text,
                    user=user.username,
                    camera=camera,
                    date=datetime.now(),
                    image=image,
                )
                return redirect(return_url)  # request.META.HTTP_REFERER para volver a la página de la que viene

    return render(request, 'comment.html', context)
