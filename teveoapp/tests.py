from django.test import TestCase
from django.urls import reverse
from .models import User, Comment, Camera, Like
from django.test import Client
from datetime import datetime

class ResourceTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.session = User.objects.create(session_id="test_session_id")
        self.client.cookies["session_id"] = "test_session_id"
        self.camera = Camera.objects.create(
            camera_id="CAM123",
            url="http://example.com",
            location="Test Location",
            latitude=0.0,
            longitude=0.0
        )

    def test_help(self):
        response = self.client.get(reverse('help_page'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'help.html')

    def test_config(self):
        response = self.client.get(reverse('config_page'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'config.html')

    def test_cameras(self):
        response = self.client.get(reverse('cameras_page'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'cameras_page.html')

    def test_comentario_GET(self):
        query_string = {'id': 'TESTCAMARA1'}
        response = self.client.get(reverse('comment_page'), data=query_string)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'comment.html')

    def test_single_camera(self):
        response = self.client.get(reverse('single_camera_page', args=[self.camera.camera_id]))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'single_camera_page.html')

    def test_dynamic(self):
            response = self.client.get(reverse('dynamic_page', args=[self.camera.camera_id]))
            self.assertEqual(response.status_code, 200)
            self.assertTemplateUsed(response, 'dynamic.html')

    def test_index(self):
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'index.html')


class TestUser(TestCase):
    def setUp(self):
        self.user = User.objects.create(
            session_id="testsessionid",
            font="Calibri",
            font_size="18px",
            username="TestUser",
        )
        self.client.cookies['session_id'] = self.user.session_id
    def test_config_page_post_change_user(self):
        response = self.client.post(reverse('config_page'), {
            'action': 'change-user',
            'username': 'NewUsername'
        })
        self.assertEqual(response.status_code, 200)
        self.user.refresh_from_db()
        self.assertEqual(self.user.username, 'NewUsername')

    def test_config_page_post_change_font(self):
        response = self.client.post(reverse('config_page'), {
            'action': 'change-font',
            'font': 'Arial',
            'font-size': '22px'
        })
        self.assertEqual(response.status_code, 200)
        self.user.refresh_from_db()
        self.assertEqual(self.user.font, 'Arial')
        self.assertEqual(self.user.font_size, '22px')


class LikeCameraTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create(session_id="test_session_id", username="TestUser")
        self.camera = Camera.objects.create(camera_id="CAM123", url="http://example.com", location="Test Location",
                                            latitude=0.0, longitude=0.0)

    def only_one_like(self):
        self.client.cookies['session_id'] = self.user.session_id
        response = self.client.post(reverse('like_camera', args=[self.camera.camera_id]))
        self.assertEqual(response.status_code, 200)

        self.assertTrue(Like.objects.filter(user=self.user, camera=self.camera).exists())
        self.assertEqual(Like.objects.filter(user=self.user, camera=self.camera).count(), 1)
