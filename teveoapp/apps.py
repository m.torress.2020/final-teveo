from django.apps import AppConfig


class TeVeoAppConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "teveoapp"
