# Generated by Django 5.0.3 on 2024-06-20 21:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("teveoapp", "0009_remove_camera_prefix"),
    ]

    operations = [
        migrations.AlterField(
            model_name="user",
            name="font",
            field=models.TextField(default="Calibri"),
        ),
        migrations.AlterField(
            model_name="user",
            name="font_size",
            field=models.TextField(default="13px"),
        ),
    ]
