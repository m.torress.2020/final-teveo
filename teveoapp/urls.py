from django.urls import path
from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("camaras", views.cameras_page, name="cameras_page"),
    path("image_embedded/<str:camera_id>", views.image_embedded, name="image_embedded"),
    path("comment_dyn/<str:camera_id>", views.comment_dyn, name="comment_dyn"),
    path("help", views.help_page, name="help_page"),
    path("config", views.config_page, name="config_page"),
    path("comentario", views.comment_page, name="comment_page"),
    path("logout", views.logout, name="logout"),
    path("<str:camera_id>-dyn", views.dynamic_page, name="dynamic_page"),
    #path("dyn_image_<str:camera_id>", views.dyn_image, name="dyn_image"),
    path("camara_json", views.camera_json, name="camara_json"),
    path("<str:camera_id>", views.single_camera_page, name="single_camera_page"),
]
