from django.db import models
from django.utils import timezone

class Camera(models.Model):
    camera_id = models.CharField(max_length=100,  default="0.0")
    url = models.URLField(max_length=300)
    ncomments = models.IntegerField(default=0)
    nlikes = models.IntegerField(default=0)
    location = models.CharField(max_length=300)
    latitude = models.CharField(max_length=300, default="0.0")
    longitude = models.CharField(max_length=300, default="0.0")

    def __str__(self):
        return self.camera_id


class Comment(models.Model):
    text = models.TextField()
    user = models.CharField(max_length=25)
    camera = models.ForeignKey(Camera, on_delete=models.CASCADE)
    date = models.DateTimeField()
    image = models.BinaryField(default=b'')


class User(models.Model):
    session_id = models.CharField(max_length=100)
    username = models.CharField(max_length=25, default="Anónimo")
    font = models.TextField(default="Calibri")
    font_size = models.TextField(default="24px")
    

class Like(models.Model):
    camera = models.ForeignKey(Camera, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateTimeField(default=timezone.now)
