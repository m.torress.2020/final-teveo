# Final-TeVeO

# ENTREGA CONVOCATORIA JUNIO

# ENTREGA DE PRÁCTICA

## Datos
* Nombre: Marta Torres Sánchez
* Titulación: Sistemas + ADE
* Cuenta en laboratorios: mtorress
* Cuenta URJC: m.torress.2020
* Video básico (url): https://youtu.be/crnD3nJyHW8?si=HlCABCw-On3XnAji SI NO FUNCIONA: https://vimeo.com/969766622
* Video parte opcional (url): https://youtu.be/fpiWpIu5O0M?si=OFh_0GWeqaOfFMqe SI NO FUNCIONA: https://vimeo.com/969766320
* Despliegue (url): http://mtorress.pythonanywhere.com
* Contraseñas: Practica123
* Cuenta Admin Site: teveo/Practica123

## Resumen parte obligatoria
TeVeo es una aplicación web en la que se muestran varias cámaras de tráfico españolas en tiempo real. 
Se puede comentar y dar like a las cámaras, así como ver los comentarios de cada cámara. No requiere autenticación.
Esta aplicación se ha creado usando Django y SqLite para la base de datos.

# PLANTILLA BASE
Esta plantilla base se usa en todas y cada una de las otras plantillas.
Se incluye el favicon.
En la cabecera (header) aparece el nombre de la página con una foto de fondo. También hay una barra de navegación que 
permite acceder al resto de recursos.
En el cuerpo se incluirá el contenido de cada página.
Se incluye un pie de página (footer) en el que aparece información sobre el número de cámaras y el número de comentarios.
Se utiliza bootstrap para que las páginas funcionen en varios dispositivos.
Se ulitiza css para el estilo y apariencia del sitio.

# RECURSOS
- La página principal de TeVeO muestra un listado de los comentarios ordenados de más a menos recientes junto al nombre 
de la cámara e imagen que se comenta. Pinchando en los nombres de las cámaras, se accede a la página específica de 
cada cámara. Los comentarios están paginados de 10 en 10.
- La página de cámaras muestra un listado de las cámaras con información sobre cada cámara (número de likes y 
comentarios, nombre, id, coordenadas). Aparecen las cámaras al descargar los listados. También aparece una imagen 
aleatoria. Por cada cámara, hay enlaces a la página de la cámara, a la página dinámica, y a la página JSON de cada 
cámara. Los comentarios están paginados de 2 en 2.
- La página de una cámara contiene información de la cámara, la imagen actual (ocupando un 50% del ancho de la pantalla),
enlaces a la página de poner un comentario y a la página dinámica y el listado de comentarios. Se ha añadido un botón 
para dar Me Gusta.
- La página para poner un comentario muestra un formulario para rellenar con el texto del comentario y la imagen 
que se va a comentar con la fecha. Recibe un parámetro en formato query string que es el identificador de la cámara.
- La página dinámica actualiza la imagen y los comentarios cada 30 segundos usando HTMX. También tiene enlace a la 
página de la cámara y a la página de poner un comentario.
- La página de configuración se utiliza para cambiar el nombre de usuario, la fuente y el tamaño de fuente. Además,
contiene un link para utilizar la información de usuario elegida en otro navegador.
- La página de ayuda muestra la documentación de la aplicación, explicando su funcionalidad.

# LISTADOS
Los 3 listados XML se parsean para sacar los datos necesarios de cada cámara: id, nombre, url, latitud y longitud.

## Lista partes opcionales
* Favicon: incluyo favicon.ico 
* Paginación de los comentarios de cada cámara específica: Aparte de paginar de 5 en 5 los comentarios de la página 
principal, he paginado de 2 en 2 los comentarios de la página específica de cada cámara.
* Cerrar sesión: Al tocar el botón logout en la barra de navegación, se cierra la sesión, volviendo los datos de usuario
a los valores por defecto.
* Se puede dar Me Gusta a las cámaras, solo un like por cámara por cada id de sesión.
* He añadido un listado 3, descargando en xml una de las bases de datos que nos propocionaba el enunciado 
(https://datos.gob.es)  y he creado otra función para procesar los datos de este listado. El listado 3 se compone de
4 cámaras de https://opendata.euskadi.eus/contenidos/ds_localizaciones/camaras_trafico/opendata/camaras-trafico.xml y 
su id de cámara comienza por LIS3-.
* He añadido más tests de los necesarios, incluyendo además para Like y User.
* Al poner un comentario, vuelves a la página en la que estabas. Es decir, si pones un comentario desde la página 
dinámica, al pulsar Enviar volverás a la página dinámica y verás tu comentario, al igual que si lo haces desde la 
página de la cámara volverás a esa página.
* Énfasis en la apariencia del sitio: diseño visual atractivo y coherente (uniforme en todas las plantillas).